//
//  E_kajdankaApp.swift
//  E-kajdanka
//
//  Created by Student on 23.01.2024..
//

import SwiftUI

@main
struct E_kajdankaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
